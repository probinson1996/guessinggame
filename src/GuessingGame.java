package src;

public class GuessingGame {
	public static int binarySearch(int[] a, int key)
	{
		int low = 0;
	    int mid = a.length / 2; 															
		int high = a.length -1;
		
	    while (low <= high) 
	    {
	        mid = ((high - low) / 2) + low;											
	        if (a[mid] > key) 
	            high = mid - 1;
	        else if (a[mid] < key) 
	            low = mid + 1;
	        else 
	            break;
	    }
	    
	    if (a[mid] == key)
	    	return mid;
	    else
	    	return -1;
	}}
