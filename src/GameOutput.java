package src;

public class GameOutput 
{

	static int[] a;
	static int key;
	public static void main(String[] args) 
	{
		a = new int[]{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,3,4,5,6,7,8,9,10,11,12,13,14};
		key = 2;
		System.out.println(binarySearch(a, key));
	
	
	
	
	}
	
	
	
	
	
	public static int binarySearch(int[] a, int key)
	{
		int low = 0;
	    int mid = a.length / 2; 															
		int high = a.length -1;
		
	    while (low <= high) 
	    {
	        mid = ((high - low) / 2) + low;											
	        if (a[mid] > key) 
	            high = mid - 1;
	        else if (a[mid] < key) 
	            low = mid + 1;
	        else 
	            break;
	    }
	    
	    if (a[mid] == key)
	    	return mid;
	    else
	    	return -1;
	}
	
	
	public static int binarySearchRecursive(int[]a, int key)
	{
		int low = 0;
	    int mid = a.length / 2; 															
		int high = a.length -1;
		
		if (low <= high)
			return -1;
		else if (a[mid] > key) 
			binarySearchRecursive()
		else if (a[mid] < key) 
			binarySearchRecursive()
		
		return key;
		
	}

}
